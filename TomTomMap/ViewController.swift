//
//  ViewController.swift
//  TomTomMap
//
//  Created by Szczepanczyk on 23/09/15.
//  Copyright (c) 2015 Szczepanczyk. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet var TomTomMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        TomTomMap.delegate = self;
        
        let tomtomOverlay = MKTileOverlay(URLTemplate: "https://api.tomtom.com/map/1/tile/basic/main/{z}/{x}/{y}.png?key=hu322eyk82wt9anpgnp3ervx");
        let tomtomTraffic = MKTileOverlay(URLTemplate: "https://api.tomtom.com/lbs/map/3/traffic/s3/{z}/{x}/{y}.png?key=toronto_trafficjam&t=-1");
        
        tomtomOverlay.canReplaceMapContent = true;
        
        TomTomMap.addOverlay(tomtomOverlay, level: MKOverlayLevel.AboveLabels);
        TomTomMap.addOverlay(tomtomTraffic, level: MKOverlayLevel.AboveLabels);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        return MKTileOverlayRenderer(overlay: overlay);
    }

}

